/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/r3989/rpc_library/cmd"
)

// mycmdCmd represents the mycmd command
var testCmd = &cobra.Command{
	Use:   "test",
	Short: "测试",
	Long:  `测试`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("this is a test")
	},
}

func init() {
	cmd.AddCommand(testCmd)
}
