package route

import (
	"gitlab.com/r3989/rpc_template/src/business"

	"gitlab.com/r3989/rpc_library/rpc"
)

func init() {
	rpc.AddAllMethods(&business.TestBusiness{}, rpc.RpcOptions{NameSpace: "test"})
}
