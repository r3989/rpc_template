package main

import (
	_ "gitlab.com/r3989/rpc_template/src/cmd"
	_ "gitlab.com/r3989/rpc_template/src/route"

	"gitlab.com/r3989/rpc_library/service"
)

func main() {
	service.Do()
}
