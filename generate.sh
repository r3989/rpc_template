#!/bin/sh
dir=./
module=
while getopts ":d:m:" opt
do
    case $opt in
        d)
        dir=$OPTARG
        ;;
        m)
        module=$OPTARG
        ;;
        ?)
        echo "Usage: generate.sh -m module_name -d dir_name"
        exit 1
        ;;
    esac
done
cmd="git clone git@gitlab.com:r3989/rpc_template.git $dir"
echo $cmd
eval $cmd
cmd="grep \"gitlab.com/r3989/rpc_template\" -rl $dir | xargs sed -i \"\" \"s#gitlab.com/r3989/rpc_template#$module#g\""
echo $cmd
eval $cmd
echo "init finished"
